package com.example.practica03kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener{ ingresar() }
        btnSalir.setOnClickListener{ salir() }
    }
    private fun iniciarComponentes(){
        btnIngresar=findViewById(R.id.btnIng)
        btnSalir=findViewById(R.id.btnSalir)
        txtUsuario=findViewById(R.id.txtUsuario)
        txtContraseña=findViewById(R.id.txtContraseña)
    }

    private fun ingresar(){
        val strUsuario:String
        val strContraseña:String

        strUsuario=applicationContext.resources.getString(R.string.usuario)
        strContraseña=applicationContext.resources.getString(R.string.contraseña)

        if(strUsuario.equals(txtUsuario.text.toString()) && strContraseña.equals(txtContraseña.text.toString())){
            // Hacer el paquete para enviar información
            val bundle = Bundle()
            bundle.putString("usuario",txtUsuario.text.toString())

            // Hacer intent para llamar otra actividad
            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)

            // Iniciar la actividad esoerando o no respuesta
            startActivity(intent)


        } else{
            Toast.makeText(this.applicationContext,"Compruebe el usuario y/o contraseña", Toast.LENGTH_SHORT).show()
        }
    }

    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Salir de la app")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }
}