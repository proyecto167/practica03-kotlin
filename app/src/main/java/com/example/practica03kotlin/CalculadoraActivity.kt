package com.example.practica03kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

private lateinit var btnSumar:Button
private lateinit var btnRestar:Button
private lateinit var btnMulti:Button
private lateinit var btnDiv:Button
private lateinit var btnLimpiar:Button
private lateinit var btnRegresar:Button
private lateinit var lblUsuario: TextView
private lateinit var lblResultado: TextView
private lateinit var txtUno: EditText
private lateinit var txtDos: EditText

// Declarar el objeto de la calculadora
private var calculadora = Calculadora(0, 0)

class CalculadoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()
        var datos = intent.extras
        var usuario=datos!!.getString("usuario")
        lblUsuario.text=usuario.toString()
        btnSumar.setOnClickListener{btnSumar()}
        btnRestar.setOnClickListener{btnRestar()}
        btnMulti.setOnClickListener{btnMulti()}
        btnDiv.setOnClickListener{btnDiv()}
        btnLimpiar.setOnClickListener{btnLimpiar()}
        btnRegresar.setOnClickListener{btnRegresar()}
    }

    private fun iniciarComponentes(){
        btnSumar=findViewById(R.id.btnSumar)
        btnRestar=findViewById(R.id.btnRestar)
        btnMulti=findViewById(R.id.btnMultiplicar)
        btnDiv=findViewById(R.id.btnDividir)
        btnLimpiar=findViewById(R.id.btnLimpiar)
        btnRegresar=findViewById(R.id.btnRegresar)
        lblUsuario=findViewById(R.id.lblUsuario)
        lblResultado=findViewById(R.id.lblResultado)
        txtUno=findViewById(R.id.txtNum1)
        txtDos=findViewById(R.id.txtNum2)
    }

    fun btnSumar(){
        val num1Text = txtUno.text.toString()
        val num2Text = txtDos.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1= num1Text.toInt()
            calculadora.num2= num2Text.toInt()

            val total = calculadora.suma()
            lblResultado.text = total.toString()
        } else {
            Toast.makeText(this.applicationContext,"Ingresa todos los campos", Toast.LENGTH_SHORT).show()
        }

    }

    fun btnRestar(){
        val num1Text = txtUno.text.toString()
        val num2Text = txtDos.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1= num1Text.toInt()
            calculadora.num2= num2Text.toInt()

            val total = calculadora.resta()
            lblResultado.text = total.toString()
        } else {
            Toast.makeText(this.applicationContext,"Ingresa todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

    fun btnMulti(){
        val num1Text = txtUno.text.toString()
        val num2Text = txtDos.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1= num1Text.toInt()
            calculadora.num2= num2Text.toInt()

            val total = calculadora.multiplicacion()
            lblResultado.text = total.toString()
        } else {
            Toast.makeText(this.applicationContext,"Ingresa todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

    fun btnDiv(){
        val num1Text = txtUno.text.toString()
        val num2Text = txtDos.text.toString()

        if (num1Text.isNotEmpty() && num2Text.isNotEmpty()) {
            calculadora.num1= num1Text.toInt()
            calculadora.num2= num2Text.toInt()

            val total = calculadora.division()
            lblResultado.text = total.toString()
        } else {
            Toast.makeText(this.applicationContext,"Ingresa todos los campos", Toast.LENGTH_SHORT).show()
        }
    }

    fun btnLimpiar(){
        lblResultado.text=""
        txtUno.setText("")
        txtDos.setText("")
    }
    fun btnRegresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("Regresar al MainActivity")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }



}